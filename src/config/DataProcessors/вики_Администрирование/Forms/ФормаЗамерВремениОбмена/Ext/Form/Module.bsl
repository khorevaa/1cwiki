﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	вики_ДиалогСервер.ФормаАдминистративнаяПриСоздании(ЭтаФорма, Отказ, СтандартнаяОбработка);
	СтраницыКоличество	= 5;
	
КонецПроцедуры

// Выполнение замера времени обмена с сайтом
//
// Параметры: 
//
&НаСервере 
Процедура ЗамерВыполнитьНаСервере()
	
	Соединение	= вики_Сайт.СоединениеПолучить();
	Если Соединение <> Неопределено И ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Замер			= вики_Сайт.ПроизводительностьЗамерить(Соединение, СтраницыКоличество);
		ВремяСоздание	= он_Система.ВремяЗамерПредставление(Замер.Создание);
		ВремяИзменение	= он_Система.ВремяЗамерПредставление(Замер.Изменение);
		ВремяУдаление	= он_Система.ВремяЗамерПредставление(Замер.Удаление);
	КонецЕсли; 
	
	Если НЕ ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Сообщение	= Новый СообщениеПользователю;
		Сообщение.Текст = Соединение.ОшибкаТекст;
		Сообщение.Сообщить(); 
		Соединение.ОшибкаТекст	= "";
	КонецЕсли; 
	
КонецПроцедуры //ЗамерВыполнитьНаСервере 

// По команде Выполнить замер
// 
&НаКлиенте
Процедура ЗамерВыполнить(Команда)
	
	Если НЕ СтраницыКоличество Тогда
		Предупреждение("Необходимо указать количество страниц!");
	КонецЕсли;
	
	ЗамерВыполнитьНаСервере();
	
КонецПроцедуры

