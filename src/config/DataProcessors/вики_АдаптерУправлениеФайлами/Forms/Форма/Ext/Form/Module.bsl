﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ВыборЭто	= он_ОбъектыПрикладные.СвойствоПолучить(ЭтаФорма.Параметры, "РежимВыбора", Ложь);
	Если ВыборЭто Тогда
		Элементы.ФормаВыбрать.КнопкаПоУмолчанию	= Истина;
	Иначе
		Элементы.ФормаВыбрать.Видимость			= Ложь;
		Элементы.ФормаЗакрыть.КнопкаПоУмолчанию	= Истина;
	КонецЕсли;
	
	АдминистраторЭто	= вики_ПользователиСервер.РольАдминистраторЕсть();
	вики_ДиалогСервер.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	ФайлыТаблицаЗаполнитьНаСервере();
	
КонецПроцедуры

// Заполнение таблицы файлов
//
// Параметры: 
//
&НаСервере 
Процедура ФайлыТаблицаЗаполнитьНаСервере()
	
	ФайлыТаблица.Очистить();
	Соединение	= вики_Сайт.СоединениеПолучить();
	
	Если ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Для Каждого Описание Из вики_Сайт.ФайлыТаблица(Соединение) Цикл
			ФайлыСтрока	= ФайлыТаблица.Добавить();
			ФайлыСтрока.Имя						= Описание.Имя;
			ФайлыСтрока.Страница				= Описание.Страница;
			ФайлыСтрока.Размер					= Описание.Размер;
			ФайлыСтрока.Пользователь			= Описание.Пользователь;
			ФайлыСтрока.Дата					= Описание.Дата;
			ФайлыСтрока.Адрес					= Описание.Адрес;
			ФайлыСтрока.Комментарий				= Описание.Комментарий;
		КонецЦикла;
		ФайлыТаблица.Сортировать("Комментарий, Имя");
	КонецЕсли;
	
	Если НЕ ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Сообщение	= Новый СообщениеПользователю;
		Сообщение.Текст = Соединение.ОшибкаТекст;
		Сообщение.Сообщить(); 
		Соединение.ОшибкаТекст	= "";
	КонецЕсли;
	
КонецПроцедуры //ФайлыТаблицаЗаполнитьНаСервере 

// Выполняет чтение данных файла.Возвращает успех выполнения
//
// Параметры: 
// 	СтрокаИдентификатор:Число - идентификатор строки таблицы файлов
//
&НаСервере 
Функция ФайлыТаблицаДанныеПрочитатьНаСервере(СтрокаИдентификатор)
	
	ФайлыСтрока		= ФайлыТаблица.НайтиПоИдентификатору(СтрокаИдентификатор);
	Соединение		= вики_Сайт.СоединениеПолучить();
	Данные			= Неопределено;
	Если Соединение <> Неопределено Тогда
		Данные	= вики_Сайт.ФайлДанные(Соединение, ФайлыСтрока.Адрес);
	КонецЕсли;
	
	Если НЕ ПустаяСтрока(Соединение.ОшибкаТекст) Тогда
		Сообщение	= Новый СообщениеПользователю;
		Сообщение.Текст = Соединение.ОшибкаТекст;
		Сообщение.Сообщить(); 
		Соединение.ОшибкаТекст	= "";
	КонецЕсли;
	
	ФайлыСтрока.Данные		= Новый Картинка(Данные);
	ФайлыСтрока.ДанныеЕсть	= Данные <> Неопределено;
	
	Возврат ПустаяСтрока(Соединение.ОшибкаТекст);
	
КонецФункции //ФайлыТаблицаДанныеПрочитатьНаСервере 

// Удаление файлов
//
// Параметры: 
// 	ИдентификаторыМассив
//
// Возвращаемое значение: 
// 	Массив - идентификаторы удаленных строк таблицы файлов
//
&НаСервере 
Функция ФайлыУдалитьНаСервере(ИдентификаторыМассив) 
	
	Результат	= Новый Массив;
	Соединение	= вики_Сайт.СоединениеПолучить();
	
	Для Каждого Идентификатор Из ИдентификаторыМассив Цикл
		ФайлыСтрока	= ФайлыТаблица.НайтиПоИдентификатору(Идентификатор);
		Если вики_Сайт.СтраницаУдалить(Соединение, ФайлыСтрока.Страница) Тогда
			Результат.Добавить(Идентификатор);
		Иначе
			Сообщение	= Новый СообщениеПользователю;
			Сообщение.Текст = "Ошибка при удалении страницы " + ФайлыСтрока.Страница + ": " + Соединение.ОшибкаТекст;
			Сообщение.Сообщить(); 
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции //ФайлыУдалитьНаСервере 

//
// Клиент
// 

// Предопределенный метод
// 
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ЭлементыПараметрыУстановить();
	
КонецПроцедуры

// Устанавливает параметры элементов формы
//
// Параметры: 
//
&НаКлиенте 
Процедура ЭлементыПараметрыУстановить()
	
	Элементы.ГруппаПредпросмотрКартинка.Видимость	= ПредпросмотрИспользовать;
	
КонецПроцедуры //ЭлементыПараметрыУстановить 

// При нажатии динамической кнопки вики
//
// Параметры:
// 	Элемент
//
&НаКлиенте
Процедура Подключаемый_вики_КнопкаПриНажатии(Элемент)
	
	вики_ДиалогКлиент.КнопкаПриНажатии(ЭтаФорма, Элемент);
	
КонецПроцедуры //Подключаемый_вики_КнопкаПриНажатии 

// При изменении флага использования предпросмотра
// 
&НаКлиенте
Процедура ПредпросмотрИспользоватьПриИзменении(Элемент)
	
	ЭлементыПараметрыУстановить();
	
КонецПроцедуры

// По команде перечитать таблицу файлов
// 
&НаКлиенте
Процедура ФайлыТаблицаПеречитать(Команда)
	
	ФайлыТаблицаЗаполнитьНаСервере();
	
КонецПроцедуры

// Устанавливает данные предпросмотра 
//
// Параметры: 
//
&НаКлиенте
Процедура ПредпросмотрДанныеУстановить(Перечитать)
	
	Адрес	= "";
	
	ДанныеТекущие	= Элементы.ФайлыТаблица.ТекущиеДанные;
	Если ПредпросмотрИспользовать И ДанныеТекущие <> Неопределено Тогда
		Если Перечитать ИЛИ НЕ ДанныеТекущие.ДанныеЕсть Тогда
			ФайлыТаблицаДанныеПрочитатьНаСервере(Элементы.ФайлыТаблица.ТекущаяСтрока);
		КонецЕсли;
		Адрес	= ПоместитьВоВременноеХранилище(ДанныеТекущие.Данные, ЭтаФорма.УникальныйИдентификатор)
	КонецЕсли;
	
	ПредпросмотрАдрес	= Адрес;
	
КонецПроцедуры //ПредпросмотрДанныеУстановить 

// При активации строки файлов
// 
&НаКлиенте
Процедура ФайлыТаблицаПриАктивизацииСтроки(Элемент)
	
	ПредпросмотрДанныеУстановить(Ложь);
	
КонецПроцедуры

// По команде открыть страницу файла
// 
&НаКлиенте
Процедура ФайлСтраницаОткрыть(Команда)
	
	ДанныеТекущие	= Элементы.ФайлыТаблица.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		вики_ДиалогКлиент.ФормаБраузерОткрыть(вики_Страница.Ссылка(ДанныеТекущие.Страница));
	КонецЕсли;
	
КонецПроцедуры

// По команде обновить данные предпросмотра
// 
&НаКлиенте
Процедура ПредпросмотрОбновить(Команда)
	
	ПредпросмотрДанныеУстановить(Истина);
	
КонецПроцедуры

// Перед удалением файла изображения
// 
&НаКлиенте
Процедура ФайлыТаблицаПередУдалением(Элемент, Отказ)
	
	Отказ	= Истина;
	
	Если АдминистраторЭто Тогда
		ФайлыИмена	= "";
		Выбор		= Элементы.ФайлыТаблица.ВыделенныеСтроки;
		Для Каждого Идентификатор Из Выбор Цикл
			ФайлыИмена	= ФайлыИмена + Символы.ПС + ФайлыТаблица.НайтиПоИдентификатору(Идентификатор).Имя;
		КонецЦикла;
		
		он_ДиалогКлиент.ВопросПоказать(ЭтаФорма
		, "ФайлыТаблицаПереУдалениемЗавершение"
		, "Вы действительно хотите удалить выбранный(е) файл(ы)?"
		+ ФайлыИмена
		, РежимДиалогаВопрос.ДаНет
		, 
		, 
		, 
		, 
		, Выбор);
	Иначе
		он_ДиалогКлиент.ПредупреждениеПоказать("Недостаточно прав доступа! Удаление доступно только администраторам Вики.");
	КонецЕсли;
	
	
КонецПроцедуры

// Завершение диалога удаления файлов
//
// Параметры: 
//
&НаКлиенте 
Процедура ФайлыТаблицаПереУдалениемЗавершение(Результат, ОповещениеОписание) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		УдалитьМассив	= Новый Массив;
		Для Каждого Идентификатор Из ФайлыУдалитьНаСервере(ОповещениеОписание) Цикл
			УдалитьМассив.Добавить(ФайлыТаблица.НайтиПоИдентификатору(Идентификатор));
		КонецЦикла;
		Для Каждого ФайлыСтрока Из УдалитьМассив Цикл
			ФайлыТаблица.Удалить(ФайлыСтрока);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры //ФайлыТаблицаПереУдалениемЗавершение 

// Перед началом добавления строки таблицы файлов
// 
&НаКлиенте
Процедура ФайлыТаблицаПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ	= Истина;
	
	ИменаМассив	= Новый Массив;
	Для Каждого ФайлыСтрока Из ФайлыТаблица Цикл
		ИменаМассив.Добавить(ФайлыСтрока.Имя);
	КонецЦикла;
	
	ФайлДиалогДобавлениеИзменениеОткрыть(Новый Структура("ИзменениеЭто, ИменаМассив"
	, Ложь
	, ИменаМассив));
	
КонецПроцедуры

// Изменение данных строки таблицы файлов
//
// Параметры: 
// 	ДанныеТекущие
//
&НаКлиенте 
Процедура ФайлыТаблицаСтрокаИзменить(ДанныеТекущие)
	
	Если АдминистраторЭто Тогда
		ФайлДиалогДобавлениеИзменениеОткрыть(Новый Структура("ИзменениеЭто, ФайлИмя, Комментарий"
		, Истина
		, ДанныеТекущие.Имя
		, ДанныеТекущие.Комментарий));
	Иначе
		он_ДиалогКлиент.ПредупреждениеПоказать("Изменение файлов (изображений) доступно только администраторам Вики.");
	КонецЕсли; 
	
КонецПроцедуры //ФайлыТаблицаСтрокаИзменить 

// Перед началом изменения файла
// 
&НаКлиенте
Процедура ФайлыТаблицаПередНачаломИзменения(Элемент, Отказ)
	
	Отказ			= Истина;
	Выбрать(Неопределено);
	
КонецПроцедуры

// Открывает диалог добавления/изменения файла
//
// Параметры: 
// 	ФормаПараметры
//
&НаКлиенте 
Процедура ФайлДиалогДобавлениеИзменениеОткрыть(ФормаПараметры)
	
	он_ДиалогКлиент.ФормаОткрытьМодально(ЭтаФорма
	, "ФайлДиалогДобавлениеИзменениеЗавершение"
	, "Обработка.вики_АдаптерУправлениеФайлами.Форма.ФормаДобавленияФайла"
	, ФормаПараметры
	, ЭтаФорма);
	
КонецПроцедуры //ФайлДиалогДобавлениеИзменениеОткрыть 

// По завершению диалога добавления/изменения файла
//
// Параметры: 
// 	Результат
// 	ОповещениеПараметры
//
&НаКлиенте 
Процедура ФайлДиалогДобавлениеИзменениеЗавершение(Результат, ОповещениеПараметры) Экспорт
	
	Если Результат <> Неопределено Тогда
		
		// Либо добавляем строку, либо изменяем существующую
		Поиск	= ФайлыТаблица.НайтиСтроки(Новый Структура("Имя", Результат.Имя));
		Если Поиск.Количество() Тогда
			ФайлыСтрока	= Поиск[0];
		Иначе
			ФайлыСтрока	= ФайлыТаблица.Добавить();
			ФайлыСтрока.Имя			= Результат.Имя;
			ФайлыСтрока.Страница	= Результат.Страница;
		КонецЕсли;
		
		ФайлыСтрока.Пользователь	= Результат.Пользователь;
		ФайлыСтрока.Размер			= Результат.Размер;
		ФайлыСтрока.Адрес			= Результат.Адрес;
		ФайлыСтрока.Дата			= Результат.Дата;
		ФайлыСтрока.Комментарий		= Результат.Комментарий;
		
		ФайлыТаблица.Сортировать("Комментарий, Имя");
		Элементы.ФайлыТаблица.ТекущаяСтрока	= ФайлыСтрока.ПолучитьИдентификатор();
		ПредпросмотрДанныеУстановить(Истина);
	КонецЕсли;
	
КонецПроцедуры //ФайлДиалогДобавлениеИзменениеЗавершение 

// Возвращает соответствие имен и адресов файлов списком
//
// Параметры: 
//
// Возвращаемое значение: 
// 	СписокЗначений
//
&НаКлиенте 
Функция ИменаАдресаСписок()
	
	Результат	= Новый СписокЗначений;
	
	Для Каждого ФайлыСтрока Из ФайлыТаблица Цикл
		Результат.Добавить(ФайлыСтрока.Имя, ФайлыСтрока.Адрес);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции //ИменаАдресаСписок 

// По команде Выбрать
// 
&НаКлиенте
Процедура Выбрать(Команда)
	
	ДанныеТекущие	= Элементы.ФайлыТаблица.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		
		Если ВыборЭто Тогда
			// Если неудачное чтение данных, то 
			// операцию выбора НЕ производим
			Если НЕ ДанныеТекущие.ДанныеЕсть Тогда
				Если НЕ ФайлыТаблицаДанныеПрочитатьНаСервере(Элементы.ФайлыТаблица.ТекущаяСтрока) Тогда
					Возврат;
				КонецЕсли;
			КонецЕсли;
			
			ЭтаФорма.Закрыть(Новый Структура("Имя, Страница, Размер, Пользователь, Дата, Адрес, Комментарий, Данные, ИменаАдресаСписок"
			, ДанныеТекущие.Имя
			, ДанныеТекущие.Страница
			, ДанныеТекущие.Размер
			, ДанныеТекущие.Пользователь
			, ДанныеТекущие.Дата
			, ДанныеТекущие.Адрес
			, ДанныеТекущие.Комментарий
			, ДанныеТекущие.Данные.ПолучитьДвоичныеДанные()
			, ИменаАдресаСписок()));
		Иначе
			ФайлыТаблицаСтрокаИзменить(ДанныеТекущие);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// По команде сохранить файл на диск
// 
&НаКлиенте
Процедура ФайлыТаблицаСохранить(Команда)
	
	ДанныеТекущие	= Элементы.ФайлыТаблица.ТекущиеДанные;
	
	#Если МобильноеПриложениеКлиент Тогда
		ВызватьИсключение "Недоступно на мобильном клиенте!";
	#Иначе 
		Если ДанныеТекущие <> Неопределено 
			И (ДанныеТекущие.ДанныеЕсть 
			ИЛИ ФайлыТаблицаДанныеПрочитатьНаСервере(Элементы.ФайлыТаблица.ТекущаяСтрока)) Тогда
			
			он_ДиалогКлиент.ФайлВыборОткрыть(ЭтаФорма
			, "ФайлТаблицаСохранитьЗавершение"
			, РежимДиалогаВыбораФайла.Сохранение
			, 
			, Ложь
			, 
			, 
			, 
			, 
			, 
			, ДанныеТекущие.Данные);
			
		КонецЕсли;
	#КонецЕсли 
	
КонецПроцедуры

// Завершение выбора файла сохранения данных картинки
//
// Параметры: 
// 	Результат
// 	ОповещениеПараметры
//
&НаКлиенте 
Процедура ФайлТаблицаСохранитьЗавершение(Результат, ОповещениеПараметры) Экспорт
	
	Если Результат <> Неопределено Тогда
		ОповещениеПараметры.ПолучитьДвоичныеДанные().Записать(Результат[0]);
	КонецЕсли;
	
КонецПроцедуры //ФайлТаблицаСохранитьЗавершение 