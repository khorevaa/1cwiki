﻿//
// Общие методы работы с настройками
// 
// Схема вызовов 
// 	л4с_Настройки -> л4с_НастройкиКэшируемый -> л4с_НастройкиСервер
// 

// Возвращает наличие настройки информационной базы
//
// Параметры: 
// 	ИнформационнаяБаза
//
// Возвращаемое значение: 
// 	Булево
//
Функция ИнформационнаяБазаНастройкаЕсть(ИнформационнаяБаза) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.ИнформационнаяБазаНастройкаЕсть(ИнформационнаяБаза);
	
КонецФункции //ИнформационнаяБазаНастройкаЕсть 

// Проверяет наличие настройки логирования для устройства
//
// Параметры:
// 	Устройство
//
// Возвращаемое значение:
//   Булево
//
Функция НастройкаЕсть(ИнформационнаяБаза, Устройство) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.НастройкаЕсть(ИнформационнаяБаза, Устройство);
	
КонецФункции //НастройкаЕсть 

// Возвращает флаг ведения лога для текущего устройства
//
// Параметры:
// 	ИнформационнаяБаза
// 	Устройство
//
// Возвращаемое значение:
//   Булево
//
Функция Включено(ИнформационнаяБаза, Устройство) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.Включено(ИнформационнаяБаза, Устройство);
	
КонецФункции //Включено 

// Возвращает значение по-умолчанию уровня регистрации событий
//
// Параметры:
//
// Возвращаемое значение:
//   Перечисление.л4с_УровниСобытий
//
Функция УровеньУмолчание() Экспорт
	
	Возврат л4с_НастройкиКэшируемый.УровеньУмолчание();
	
КонецФункции //УровеньУмолчание 

// Возвращает уровень регистрации событий для устройства
//
// Параметры:
// 	ИнформационнаяБаза
// 	Устройство
//
// Возвращаемое значение:
//   Перечисление.л4с_УровниСобытий
//
Функция Уровень(ИнформационнаяБаза, Устройство) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.Уровень(ИнформационнаяБаза, Устройство);
	
КонецФункции //Уровень

// Возвращает адаптер хранилища лога лдя устройства
//
// Параметры:
// 	ИнформационнаяБаза
// 	Устройство
//
// Возвращаемое значение:
//   Строка
//
Функция Хранилище(ИнформационнаяБаза, Устройство) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.Хранилище(ИнформационнаяБаза, Устройство);
	
КонецФункции //Хранилище 

// Возвращает параметры адаптера записи логов
//
// Параметры: 
// 	ИнформационнаяБаза
// 	Устройство
//
// Возвращаемое значение: 
// 	Соответствие
//
Функция ХранилищеПараметры(ИнформационнаяБаза, Устройство) Экспорт
	
	Возврат л4с_НастройкиКэшируемый.ХранилищеПараметры(ИнформационнаяБаза, Устройство);
	
КонецФункции //ХранилищеПараметры 

// Возвращает хранилище по-умолчанию по выбранному адаптеру к конфигурации
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Справочник.он_Адаптеры
// 
Функция ХранилищеУмолчание() Экспорт
	
	Возврат л4с_НастройкиКэшируемый.ХранилищеУмолчание();
	
КонецФункции //ХранилищеУмолчание 

