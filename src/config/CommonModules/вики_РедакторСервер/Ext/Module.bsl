﻿//
// Создание команд редактирования и подготовка поля редактора HTML
//

// Инициализация поля редактора HTML документа. Создание кнопок формтирования
// 	редактора HTML и установка формтирования по-умолчанию документа
//
// Параметры:
// 	Форма
// 	ПолеHTML
// 	КоманднаяПанель
// 	ДействиеИмя
// 	ГиперссылкиВидимость
//
Процедура Инициализировать(Форма
	, Элемент
	, КоманднаяПанель
	, ДействиеИмя = Неопределено
	, ГиперссылкиВидимость = Истина) Экспорт
	
	// И назначаем свойства HTML документа по-умолчанию
	он_ОбъектыПрикладные.СвойствоУстановить(Форма
	, Элемент.ПутьКДанным
	, ПолучитьОбщийМакет("вики_РедакторШаблон").ПолучитьТекст());

	// Создаем кнопки форматирования
	ПанельКнопкиСоздать(Форма
	, Элемент.Имя
	, КоманднаяПанель
	, ДействиеИмя
	, ГиперссылкиВидимость);
	
КонецПроцедуры //Инициализировать 

// Добавляет кнопку на командную панель редактора
//
// Параметры: 
//
Процедура ПанельКнопкаДобавить(Форма, Родитель, ЭлементИмя, HTMLКоманда, Заголовок, МетодИмя, Картинка)
	
	Команда		= он_ФормаУправляемаяСервер.КомандаДобавить(Форма
	, вики_Диалог.КомандаИмяСформировать(ЭлементИмя, HTMLКоманда)
	, МетодИмя
	, Заголовок
	, 
	, Истина
	, ОтображениеКнопки.Картинка
	, Картинка);
	он_ФормаУправляемаяСервер.КнопкаКоманднойПанелиДобавить(Форма, Команда.Имя, Родитель, Команда.Имя);
	
КонецПроцедуры //ПанельКнопкаДобавить 

// Программное создание команд и кнопок редактирования HTML. Создает кнопки с именами в формате "ИмяHTMLПоля_КомандаДляБраузера"
//
// Параметры:
// 	Форма 						- Управляемая форма. Форма в которой необходимо создать кнопки редактирования HTML;
// 	ЭлементHTMLПоля 			- ПолеФормы или Строка. Элемент формы или имя реквизита поля HTML. Если передается строка ищет элемент формы по имени;
//  ЭлементГруппыКоманд 		- ГруппаФормы или Строка. !!!Вид группы - "Командная панель"!!!. Элемент ГруппаФормы или имя группы для расположения кнопок команд редактирования HTML. Если передается строка ищет элемент формы по имени;
//  ИмяПодключаемогоДействия 	- Строка. Имя группы для расположения кнопок команд редактирования HTML. Если не передается, то "Подключаемый_ДокументHTMLКомандаРедактирования";
//
Процедура ПанельКнопкиСоздать(Форма, ЭлементИмя, КоманднаяПанель, ДействиеИмя = Неопределено, ГиперссылкиВидимость = Истина)
	
	МетодИмя		= ?(ДействиеИмя = Неопределено
	, вики_ДиалогСервер.ФормаКнопкаПриНажатииМетодИмя(Форма, он_Диалог.ФормаДанныеТипИмя(Форма))
	, ДействиеИмя);
	// Начертание
	КнопкиГруппа	= он_ФормаУправляемаяСервер.ГруппаКнопокДобавить(Форма
	, ЭлементИмя + "ГруппаНачертание"
	, КоманднаяПанель);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "Bold", "Полужирный", МетодИмя, БиблиотекаКартинок.вики_Полужирный);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "Italic", "Курсив", МетодИмя, БиблиотекаКартинок.вики_Курсив);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "Underline", "Подчеркнутый", МетодИмя, БиблиотекаКартинок.вики_Подчеркнутый);
	// Верхний/Нижний индекс
	КнопкиГруппа	= он_ФормаУправляемаяСервер.ГруппаКнопокДобавить(Форма
	, ЭлементИмя + "ГруппаВерхнийНижнийИндекс"
	, КоманднаяПанель);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "superscript", "Надстрочный", МетодИмя, БиблиотекаКартинок.вики_Надстрочный);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "subscript", "Подстрочный", МетодИмя, БиблиотекаКартинок.вики_Подстрочный);
	// Списки
	КнопкиГруппа	= он_ФормаУправляемаяСервер.ГруппаКнопокДобавить(Форма
	, ЭлементИмя + "ГруппаСписки"
	, КоманднаяПанель);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "InsertUnorderedList", "Маркированный список", МетодИмя, БиблиотекаКартинок.вики_МаркированныйСписок);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, "InsertOrderedList", "Нумерованный список", МетодИмя, БиблиотекаКартинок.вики_НумерованныйСписок);
	// Изображение
	КнопкиГруппа	= он_ФормаУправляемаяСервер.ГруппаКнопокДобавить(Форма
	, ЭлементИмя + "ГруппаИзображения"
	, КоманднаяПанель);
	ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, вики_Редактор.КомандаИзображениеИмя(), "Изображение", МетодИмя, БиблиотекаКартинок.Картинка);
	// Гиперссылки
	Если ГиперссылкиВидимость Тогда
		КнопкиГруппа	= он_ФормаУправляемаяСервер.ГруппаКнопокДобавить(Форма
		, ЭлементИмя + "ГруппаГиперссылки"
		, КоманднаяПанель);
		ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, вики_Редактор.КомандаВставитьСсылкуНаМетаданныеИмя(), "Ссылка на метаданные", МетодИмя, БиблиотекаКартинок.вики_ВнутренняяСсылка);
		ПанельКнопкаДобавить(Форма, КнопкиГруппа, ЭлементИмя, вики_Редактор.КомандаВставитьПроизвольнуюСсылкуИмя(), "Произвольная ссылка", МетодИмя, БиблиотекаКартинок.вики_ВнешняяСсылка);
	КонецЕсли;
	
КонецПроцедуры //ПанельКнопкиСоздать

//
// Декорации
//

// Устанавливает текст и оформление декорации состояния
//
// Параметры:
// 	Декорация
// 	ОшибкаТекст
//
Процедура ДекорацияСостояниеТекстУстановить(Декорация, ОшибкаТекст) Экспорт
	
	Если ПустаяСтрока(ОшибкаТекст) Тогда
		Декорация.Заголовок		= "Соединение успешно установлено";
		Декорация.ЦветТекста	= WebЦвета.Зеленый;
	Иначе
		Декорация.Заголовок		= "Внимание: " + ОшибкаТекст;
		Декорация.ЦветТекста	= WebЦвета.Красный;
	КонецЕсли;
	
КонецПроцедуры //ДекорацияСостояниеТекстУстановить 

