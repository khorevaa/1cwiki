﻿
// Предопределенный метод
// 
Процедура ПередЗаписью(Отказ, Замещение)
	
	Если НЕ он_ОбъектыПрикладныеСервер.СвойствоДополнительноеПолучить(ЭтотОбъект
		, РегистрыСведений.л4с_ПраваРолейПользователей.СвойствоСлужебнаяЗаписьИмя()
		, Ложь) 
		И НЕ л4с_ПользователиСервер.РольАдминистраторЕсть() Тогда
		
		Отказ	= Истина;
		Сообщить("Запрещено изменять состав прав ролей пользователей!");
		
	КонецЕсли;
	
КонецПроцедуры
