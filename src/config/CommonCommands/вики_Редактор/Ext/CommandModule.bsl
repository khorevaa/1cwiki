﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	он_ДиалогКлиент.ФормаОткрыть("Обработка.вики_Редактор.Форма"
	, 
	, он_ДиалогКлиент.КомандаПараметрИсточник(ПараметрыВыполненияКоманды)
	, он_Система.ПолучитьУникальноеИмя()
	, он_ДиалогКлиент.КомандаПараметрОкно(ПараметрыВыполненияКоманды)
	, он_ДиалогКлиент.КомандаПараметрНавигационнаяСсылка(ПараметрыВыполненияКоманды));
КонецПроцедуры
